FROM nginx:alpine

COPY . /usr/share/nginx/html

# COPY /css /usr/share/nginx/html/css
# COPY /img /usr/share/nginx/html/img
# COPY /js /usr/share/nginx/html/js
# COPY index.html /usr/share/nginx/html/index.html

# COPY /etc/nginx/nginx.conf /etc/nginx/nginx.conf

# COPY conf /etc/nginx

# VOLUME /usr/share/nginx/html
# VOLUME /etc/nginx

# EXPOSE 8080:80
